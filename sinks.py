"""Methods for storing sound, or playing it"""

import config

import sounddevice

import soundfile


def play(sound):
    sounddevice.play(sound, config.samples_second)
    sounddevice.wait()


def _mean(sound):
    total = 0
    for sample in sound:
        total += abs(sample)
    return int(total / len(sound))


def draw(sound, period: float):
    samples_period = int(period * config.samples_second)
    for nsample in range(0, len(sound), samples_period):
        chunk = sound[nsample: nsample + samples_period]
        mean_sample = _mean(chunk)
        stars = mean_sample // 1000
        print('*' * stars)


def store(sound, path: str):
    filename = path

    soundfile.write(filename, sound, config.samples_second)
    print(f"done, saved in {filename}")
