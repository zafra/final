import config
import sources


def ampli(sound, factor: float):
    for sample in range(len(sound)):
        amplified = sound[sample] * factor
        if amplified >= config.max_amp:
            sound[sample] = config.max_amp
        elif amplified <= -config.max_amp:
            sound[sample] = -config.max_amp
        elif config.max_amp > amplified > -config.max_amp:
            sound[sample] = int(amplified)

    return sound


def reduce(sound, factor: int):
    remove = factor - 1
    while len(sound) > remove:
        sound.pop(remove)
        remove += factor - 1

    return sound


def extend(sound, factor: int):
    add = factor
    while len(sound) > add:
        sample = (sound[add - 1] + sound[add]) / 2
        sound.insert(add, int(sample))
        add += factor + 1

    return sound


def add(sound, duration: float, freq: float):
    sound2 = sources.square(duration, freq)

    for sample in range(len(sound)):
        added = sound[sample] + sound2[sample]
        if added >= config.max_amp:
            sound[sample] = config.max_amp
        elif added <= -config.max_amp:
            sound[sample] = -config.max_amp
        elif config.max_amp > added > -config.max_amp:
            sound[sample] = int(added)

    return sound
