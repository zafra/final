import sys

import sources
import sinks
import processes

if len(sys.argv) < 6 or len(sys.argv) > 12:
    print(f"Error: no se han introducido el número de parámetros correcto")
    exit(0)

source = sys.argv[1]
source_args = sys.argv[2]
sink = sys.argv[-3]
sink_args = sys.argv[-2]
dur = float(sys.argv[-1])


def main():
    if source == 'sin':
        sound = sources.sin(duration=dur, freq=float(source_args))
    elif source == 'constant':
        sound = sources.constant(duration=dur, positive=True)
    elif source == 'square':
        sound = sources.square(duration=dur, freq=float(source_args))
    else:
        sys.exit("Unknown source")

    while sys.argv:
        process = sys.argv.pop(3)

        if process == "ampli":
            process_args = sys.argv[3]
            sound = processes.ampli(sound=sound, factor=float(process_args))
        elif process == "reduce":
            process_args = sys.argv[3]
            sound = processes.reduce(sound=sound, factor=int(process_args))
        elif process == "extend":
            process_args = sys.argv[3]
            sound = processes.extend(sound=sound, factor=int(process_args))
        break

    if sink == "play":
        sinks.play(sound)
    elif sink == "draw":
        sinks.draw(sound=sound, period=float(sink_args))
    elif sink == "store":
        sinks.store(sound=sound, path="stored.wav")
    else:
        sys.exit("Unknown sink")


if __name__ == '__main__':
    main()
